;;; TBD --- a laxy evaluation framework for Emacs lisp.  -*- lexical-binding: t; -*-
;;;
;;; Commentary:
;;; TBD is inspired by the Clojure languages sequence type and closely
;;; resembles some Common Lisp code I wrote for an internal tool for my
;;; previous-day-job.
;;
;;; Things that are missing:
;;; - immutability.
;;; - structure sharing.
;;; - probably other things I haven't noticed.


;;; Code:

;; Define some primitive operations that comprise a lazy
;; sequence. Similar to cons cells, we need to return the head, tail,
;; and determine whether there are elements left in the sequence.

(require 'eieio)

;; TODO: There must be a better name for this type.
(defclass tbd-cons ()
  ((it :initarg :it))
  :documentation "This class defines a lazily evaluated sequence.")

(cl-defmacro tbd-cons (head tail-form)
  "Construct a TBD object.
The first element of the sequence is passed via HEAD.  The form
that generates the rest of the sequence, typically a TBD sequence,
are passed via TAIL-FORM."
  `(make-instance 'tbd-cons
                   :it (cons ,head (lambda () ,tail-form))))


(cl-defgeneric tbd-emptyp (seq)
  "Return a true value (i.e., not nil) if TBD sequence has no remaining elements.
SEQ is a possibly-empty TBD sequence.")

(cl-defgeneric tbd-head (seq)
  "Return the next value in a lazy sequence.
SEQ is a TBD sequence.")

(cl-defgeneric tbd-tail (seq)
  "Return the \"rest\" of a lazy sequence.
SEQ is a TBD sequence.")





(cl-defmethod tbd-emptyp ((seq tbd-cons))
  "Return a true value (i.e., not nil) if TBD sequence has no remaining elements.
OBJ is a possibly-empty TBD sequence."
  nil)

(cl-defmethod tbd-emptyp ((seq list))
  "Return a true value (i.e., not nil) if SEQ has no remaining elements.
OBJ is a possibly-empty TBD sequence."
  (null seq))

(cl-defmethod tbd-head ((seq tbd-cons))
  "Return the next value in the TBD lazy sequence SEQ."
  (car (slot-value seq 'it)))

(cl-defmethod tbd-head ((seq list))
  "Return the next value in the TBD lazy sequence SEQ."
  (car seq))

(cl-defmethod tbd-tail ((seq tbd-cons))
  "Return the \"rest\" of the lazy sequence SEQ."
  (funcall (cdr (slot-value seq 'it))))

(cl-defmethod tbd-tail ((seq list))
  "Return the \"rest\" of the lazy sequence SEQ."
  (cdr seq))

(defun tbd--unbounded-counter (n)
  "Return a lazily evaluated sequence of integers, starting with N."
  (tbd-cons n (tbd--unbounded-counter (1+ n))))


(defun tbd--range-helper (start end step)
  "Build a lazily evaluated sequence of integers.
START will be the first value in the sequence.  The sequence will
end when the value is >= END.  The values in the sequence will
increase by STEP, or decrease if STEP is negative.

If STEP is positive, but the lower bound is greater than the upper
bound, nil is returned.  Likewise STEP is negative, nil will be
returned if the lower bound is less than the upper bound."
  (when (or
         (and (> step 0) (< start end))
         (and (< step 0) (> start end)))
    (tbd-cons start (tbd--range-helper (+ start step) end step))))


(defun tbd-range (&optional a b c)
  "Build a lazily evaluated sequence of integers.

TBD-RANGE takes up to three arguments and creates the sequence
differently, depending on which arguments are present.

When called with no arguments, TBD-RANGE produces the sequence [0, ∞).

With one argument, A, the argument is the upper bound of the range [0, A).

When called with two arguments, A and B, the sequence is [A, B).

When called with all three arguments, A, B, and C, the range
produced steps through [A, B) by the interval C.

If the step (1 by default or C if present) is positive, but the
lower bound is less than the upper bound, nil is
returned.  Likewise if the step is negative, nil will be returned
if the lower bound is less than the upper bound."
  (pcase (list a b c)
    ('(nil nil nil) (tbd--unbounded-counter 0))
    (`(,end nil nil) (tbd--range-helper 0 end 1))
    (`(,start ,end nil) (tbd--range-helper start end 1))
    (`(,start ,end ,step) (tbd--range-helper start end step))))


(defun tbd-take (n seq)
  "Create a lazy sequence of the first N elements of sequence SEQ."
  (when (and (> n 0) (not (tbd-emptyp seq)))
    (tbd-cons (tbd-head seq) (tbd-take (1- n) (tbd-tail seq)))))


(defun tbd-drop (n seq)
  "Return a lazy sequence of all but the first N items in SEQ."
  (cl-loop repeat n
           for result = seq then (tbd-tail result)
           finally return result))


(defun tbd-iterate (f x)
  "Return the sequence X, (F X), (F (F X)), etc."
  (tbd-cons x (tbd-iter f (funcall f x))))


(defun tbd-filter (p seq)
  "Filter a lazy sequence.

Return a lazy sequence of the elements SEQ for which P, a
predicate function, returns true."
  (unless (tbd-emptyp seq)
    (let ((value (tbd-head seq)))
      (if (funcall p value)
          (tbd-cons value (tbd-filter p (tbd-tail seq)))
        (tbd-filter p (tbd-tail seq))))))


;; TODO: This should be in some kind of utilities package.
(defun find-if (p list)
  "Return the first element of LIST for which (P element) is true."
  (while (and list (not (funcall p (car list))))
    (pop list))
  (car list))


;; TODO: This should be in some kind of utilities package.
(defun tbd--find-if (p list)
  "Return the first element of LIST for which P is true."
  (while (and list (not (funcall p (car list))))
    (pop list))
  (car list))

(defun tbd--mapcar-helper (f seqs)
  "Lazily apply F to the next elements in SEQS.

This is a helper function to avoid repetitive consing."
  (unless (tbd--find-if #'tbd-emptyp seqs)
    (tbd-cons (apply f (mapcar #'tbd-head seqs))
              (tbd--mapcar-helper
               f
               (mapcar #'tbd-tail seqs)))))

(defun tbd-mapcar (f seq &rest seqs)
  "Lazily apply F to the elements of SEQS.

Create a new TBD sequence which resembles ((f a1 b1 c1) (f a2 b2
c2) (f a3 b3 c3)...) for the input sequence a, b, c."
  (tbd--mapcar-helper f (cons seq seqs)))

(defun tbd-to-list (seq &optional max tellp)
  "Transform a lazy sequence to a list.

Return a list consisting of the elements of the sequence
SEQ.  Optionally, and probably wisely, limiting the number of
elements in the list to MAX.

If the argument TELLP is not nil, return a list the first element
of which is the list extracted from SEQ.  The second element will
be logically true if all elements of the sequence are in the
returned list, and false (nil) if the list comprises only a
subset of the sequence's elements."

  (cl-loop
   for sequence = seq then (tbd-tail sequence)
   for added from 0
   while (and (not (tbd-emptyp sequence))
              (or (null max) (< added max)))
   collect (tbd-head sequence) into result
   finally (return
            (if tellp
                (list result (tbd-emptyp sequence))
              result))))

;;; tbd.el ends here.

